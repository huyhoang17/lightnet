Models
======
.. automodule:: lightnet.models

Networks
--------
.. autoclass:: lightnet.models.Darknet19
.. autoclass:: lightnet.models.Yolo
.. autoclass:: lightnet.models.TinyYolo
.. autoclass:: lightnet.models.MobileNetYolo

Data
----
.. autoclass:: lightnet.models.DarknetData
   :members:


.. include:: ../links.rst
