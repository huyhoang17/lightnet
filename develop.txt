# Runtime dependencies
git+https://gitlab.com/EAVISE/brambox

# Development dependencies
sphinx
sphinx_rtd_theme
recommonmark

-e .[visual]
